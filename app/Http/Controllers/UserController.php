<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Entry;

class UserController extends Controller
{
    public function __construct()
    {
       // $this->middleware('auth');
    }

    public function show($user)
    {   
        $entries = Entry::where('user_id',$user)->get();
        $usuario = User::where('username', $user)->get();
        $entries = Entry::where('user_id',$usuario[0]->id)->get();
        
       // $entries = Entry::where('user_id',auth()->id())->get();
       //$user = $usuario;
       //var_dump($usuario);
        //echo (($usuario));
       //return view('users.show', ['usuario' => $usuario],'entries');
       return view('users.show', compact('usuario','entries'));
       //return view('home');
    }
   
}
