<?php

namespace App\Exceptions;
use Exception;
use App\Entry;
class InvalidEntrySlugException  extends \Exception
{   
   private $entry;
   public function __construct(Entry $entry)
   {

   //dd($entry);
    $this->entry=$entry;

   //parent::__contruct()

   }
   public function render()
   {    
       //dd($this->entry);
       return redirect($this->entry->getUrl());
   }
   
}
