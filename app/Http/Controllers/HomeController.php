<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entry ;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

     
    public function index()
    {
        $entries = Entry::where('user_id',auth()->id())->get();
        //dd($entries);
        return view('home',compact('entries'));
    }
}
